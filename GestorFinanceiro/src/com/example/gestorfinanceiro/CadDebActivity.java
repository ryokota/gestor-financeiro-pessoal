package com.example.gestorfinanceiro;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class CadDebActivity extends Activity {
	DAO dao;
	Spinner spinner1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cad_deb);
		
		dao = new DAO(this);
		dao.abrir();
		
		spinner1 =(Spinner)findViewById(R.id.spnCategoria);
		SimpleCursorAdapter ad = new SimpleCursorAdapter(this,
									android.R.layout.simple_list_item_1,
									dao.listarCategoria(),
									new String[]{"categoria"},
									new int[]{android.R.id.text1},0);
		spinner1.setAdapter(ad);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.cad_deb, menu);
		return true;
	}
	
	public void onClick(View v){
		
		switch(v.getId()){
		case R.id.btnSalvar:
			
			EditText edtDesc = (EditText)findViewById(R.id.edtDesc);
			EditText edtVal  = (EditText)findViewById(R.id.edtVal);
			EditText edtDat  = (EditText)findViewById(R.id.edtDat);
			
			if (edtDesc.getText().toString().equals("")){
				Toast.makeText(this,"Preencha todos os campos !", Toast.LENGTH_SHORT).show();
				break;
			}
			if (edtVal.getText().toString().equals("")){
				Toast.makeText(this,"Preencha todos os campos !", Toast.LENGTH_SHORT).show();
				break;
			}
			if (edtDat.getText().toString().equals("")){
				Toast.makeText(this,"Preencha todos os campos !", Toast.LENGTH_SHORT).show();
				break;
			}
			
			String desc = edtDesc.getText().toString();
			String dat  = edtDat.getText().toString();
			Double val  = Double.parseDouble(edtVal.getText().toString());
			
			dao.cadastrarFinanca(desc, dat, 2, (int)spinner1.getSelectedItemId(), val);
			
			Toast.makeText(this, "Cadastrado com Sucesso!", 
					Toast.LENGTH_SHORT).show();
			
			
			this.finish();
			break;
		
		case R.id.btnSair:
			this.finish();
			break;
		
		}
		
	}

}
