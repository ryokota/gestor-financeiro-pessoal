package com.example.gestorfinanceiro;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class AlterarRegistroActivity extends Activity {
	private long id;
	private DAO dao;
	private Spinner spnCategoria;
	private EditText edtDescricao,edtValor,edtData;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alterar_registro);
		
		Intent it = getIntent();
		id = it.getExtras().getLong("id");
		
		dao = new DAO(this);
		dao.abrir();
		
		Cursor cursor = dao.obterFinanca(id);
		cursor.moveToFirst();
		
		String desc  = cursor.getString(1);
		long valor   = cursor.getLong(5);
		String data  = cursor.getString(2); 
		int position = cursor.getInt(4); 
		
		edtDescricao = (EditText)findViewById(R.id.edtAltDescricao);
		edtValor     = (EditText)findViewById(R.id.edtAltValor);
		edtData		 = (EditText)findViewById(R.id.edtAltData );
		spnCategoria = (Spinner)findViewById(R.id.spnCategoria);
		
		
		
		edtDescricao.setText(desc);
		edtValor.setText(String.valueOf(valor));
		edtData.setText(data);
		
		SimpleCursorAdapter ad = new SimpleCursorAdapter(this,
				android.R.layout.simple_list_item_1,
				dao.listarCategoria(),
				new String[]{"categoria"},
				new int[]{android.R.id.text1},0);
			
			
		spnCategoria.setAdapter(ad);
		
		for (int i = 0; i < ad.getCount(); i++) {
			if (ad.getItemId(i) == position) {
				spnCategoria.setSelection(i);
				break;
			}
		}
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.alterar_registro, menu);
		return true;
	}
	
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		switch(v.getId()){
		
		case R.id.btnVoltar:
			
		    int id_tipo ;
		    if(id == 2)
		    	id_tipo = dao.getIdTipo("D�bito");
		    else
		    	id_tipo = dao.getIdTipo("Cr�dito");
		    
		    //nt id_categoria = dao.getIdCategoria(spnCategoria.getId());
		    //int categoria = spnCategoria.getSelectedItemPosition();
		   Toast.makeText(this, ">>>>> "+ spnCategoria.getSelectedItemId(), Toast.LENGTH_SHORT).show();
			
		    dao.alterarRegistro(id, 
					edtDescricao.getText().toString(), 
					(int)spnCategoria.getSelectedItemId(), 
					id_tipo, 
					edtValor.getText().toString(), 
					edtData.getText().toString());
		    finish();
			break;
			
		case R.id.btnAltVoltar:
			this.finish();
			break;
			
			
		}
		//this.finish();
	}

}
