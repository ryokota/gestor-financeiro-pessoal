package com.example.gestorfinanceiro;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.ListView;

public class ListaDebito extends ListActivity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		DAO dao = new DAO(this);
		dao.abrir();
		
		SimpleCursorAdapter adaptador = new SimpleCursorAdapter(this,
				R.layout.financas_activity,dao.listarDebito(), 
				new String[]{"descricao","data","valor"}, 
				new int[]{R.id.txtDescricao,R.id.txtData,R.id.txtValor});
		
		setListAdapter(adaptador);
		getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		dao.fechar();
		
	}
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		
		Intent it = new Intent();
		it = new Intent(this,AlterarRegistroActivity.class);
		it.putExtra("id", id);
		startActivity(it);
	}
}
