package com.example.gestorfinanceiro;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class OpenHelper extends SQLiteOpenHelper{

	public OpenHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		String tabela = "CREATE TABLE financas ( "+
				"_id INTEGER PRIMARY KEY AUTOINCREMENT,"+
				"descricao TEXT,"+
				"data TEXT,"+
				"tipo INTEGER,"+ // 2 para debito ,1 para credito 
				"categoria INTEGER,"+
				"valor REAL)";
		
		String tabela2 = "CREATE TABLE TabelaCategoria (_id INTEGER PRIMARY KEY AUTOINCREMENT,categoria TEXT)";
		String tabela3 = "CREATE TABLE TabelaTipo (_id INTEGER PRIMARY KEY AUTOINCREMENT ,tipo TEXT)";
		
		
		
		String cmd  = "INSERT INTO TabelaCategoria (categoria)VALUES('Financiamentos')";
		String cmd2 = "INSERT INTO TabelaCategoria (categoria)VALUES('Salario')";
		
		String cmd3 = "INSERT INTO financas(descricao,data,tipo,categoria,valor)"+
				"VALUES('Parcela Carro','20-10-2013',2,1,500.50)";
		
		String cmd4 = "INSERT INTO financas(descricao,data,tipo,categoria,valor)"+
				"VALUES('Salario ','11-10-2013',1,2,1500.51)";
		
		String cmd5 = "INSERT INTO TabelaTipo (_id,tipo)VALUES(1,'Cr�dito')";
		String cmd6 = "INSERT INTO TabelaTipo (_id,tipo)VALUES(2,'D�bito')";
		
		db.execSQL(tabela);
		db.execSQL(tabela2);
		db.execSQL(tabela3);
		
		db.execSQL(cmd);
		db.execSQL(cmd2);
		db.execSQL(cmd3);
		db.execSQL(cmd4);
		db.execSQL(cmd5);
		db.execSQL(cmd6);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}

}
