package com.example.gestorfinanceiro;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class CadastroCategoria extends Activity {
	DAO dao;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		dao = new DAO(this);
		dao.abrir();
		
		setContentView(R.layout.activity_cadastro_categoria);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.cadastro_categoria, menu);
		return true;
	}
	
	public void onClick(View v){
		
		switch(v.getId()){
		
		case R.id.btnSalvar:
			
			EditText edtCategoria = (EditText)findViewById(R.id.edtCategoria);
			String categoria = edtCategoria.getText().toString();
			
			if (categoria.equals("")){
				Toast.makeText(this,"Preencha todos os campos !", Toast.LENGTH_SHORT).show();
				break;
			}
			dao.cadastrarCategoria(categoria);
			Toast.makeText(this, "Categoria cadastrado:"+categoria+ " com Sucesso!", 
					Toast.LENGTH_SHORT).show();
			
			edtCategoria.setText("");
			edtCategoria.requestFocus();
			this.finish();
			break;
			
		case R.id.btnSair:
			this.finish();
			break;
		
		}
		
	}
	
	
}
