package com.example.gestorfinanceiro;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class OrcamentoFinalActivity extends Activity {
	DAO dao;
	Button btnVoltar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_orcamento_final);
		
		
		dao = new DAO(this);
		dao.abrir();
		
		
		btnVoltar        = (Button)findViewById(R.id.btnVoltar2);
		btnVoltar.setOnClickListener(myhandler);
		
		TextView debito  = (TextView)findViewById(R.id.txtDebitos);
		TextView credito = (TextView)findViewById(R.id.txtCreditos);
		TextView total   = (TextView)findViewById(R.id.txtFinal);
		
		double total_credito,total_debito;
		
		total_credito   = dao.totalDebito();
		total_debito    = dao.totalCredito();
		//Toast.makeText(this, "debito:"+total_debito+ " credito: "+total_credito, Toast.LENGTH_SHORT);
		
		debito.setText(Double.toString(total_debito));
		credito.setText(Double.toString(total_credito));
		
		total.setText(Double.toString(total_debito-total_credito));
		
		
	}

	View.OnClickListener myhandler = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			finish();
			
		}
	};
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.orcamento_final, menu);
		return true;
	}
	
	

}
