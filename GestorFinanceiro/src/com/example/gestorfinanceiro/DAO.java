package com.example.gestorfinanceiro;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.widget.Toast;



public class DAO {
	private SQLiteDatabase banco;
	private OpenHelper helper;
	
	
	public DAO(Context c) {
		// TODO Auto-generated constructor stub
		helper =  new OpenHelper(c,"financa.db",null,1);
	}
	
	public void abrir() throws SQLiteException{
		banco = helper.getWritableDatabase();
	}
	
	public void fechar(){
		if(banco != null)
			banco.close();		
	}
	
	
	public void cadastrarCategoria(String categoria){
		ContentValues valor = new ContentValues();
		valor.put("categoria", categoria);
		banco.insert("TabelaCategoria", null, valor);
	}
	
	public void cadastrarFinanca(String descricao,String data,int tipo,int categoria,double valor){
		ContentValues valores =  new ContentValues();
		
		valores.put("descricao", descricao);
		valores.put("data", data);
		valores.put("tipo", tipo);
		valores.put("categoria", categoria);
		valores.put("valor", valor);
		
		banco.insert("financas",null,valores);
		
	}

	public Cursor listarFinancas(){
		
		Cursor res;
		res = banco.query("financas",null,null, null, null, null, "descricao");
			
		return res;
	}
		
	public Cursor listarCategoria(){
		return banco.rawQuery("SELECT * FROM TabelaCategoria;", null);
	}
	
	public Cursor listarCredito(){
		return banco.rawQuery("SELECT financas._id ,financas.descricao, financas.data, financas.valor, TabelaCategoria.categoria , TabelaTipo.tipo" +
				" FROM financas " +
				" JOIN  TabelaCategoria ON financas.categoria = TabelaCategoria._id " +
				" JOIN  TabelaTipo      ON financas.tipo      = TabelaTipo._id " +
				"WHERE financas.tipo = 1 ORDER BY financas.data ;",null);// 1 credito
	}
	
	public Cursor listarDebito(){
		return banco.rawQuery("SELECT financas._id ,financas.descricao, financas.data, financas.valor, TabelaCategoria.categoria , TabelaTipo.tipo" +
				" FROM financas " +
				" JOIN  TabelaCategoria ON financas.categoria = TabelaCategoria._id " +
				" JOIN  TabelaTipo      ON financas.tipo      = TabelaTipo._id " +
				"WHERE financas.tipo = 2 ORDER BY financas.data ;",null);
	}
	public double totalDebito(){
		
		double total;
		this.abrir();
		
		Cursor c = banco.rawQuery("SELECT SUM(valor) FROM financas WHERE tipo = 2", null);
		c.moveToFirst();
		total = c.getDouble(0);
		
		
		this.fechar();
		return total;
	}
	
	public double totalCredito(){
		
		double total;
		this.abrir();
		
		Cursor c = banco.rawQuery("SELECT SUM(valor) FROM financas WHERE tipo = 1", null);
		c.moveToFirst();
		total = c.getDouble(0);
		
		
		this.fechar();
		return total;
	}
	
	
	public Cursor obterFinanca(long id){
		return banco.query("financas", null, "_id = "+ id, null, null, null, null);
		
	}
	
	public void excluirFinanca(int id){
		banco.delete("financas", "_id ="+id, null);
	}
	public int getIdCategoria(String categoria){
	
		Cursor c = banco.rawQuery("SELECT _id FROM TabelaCategoria WHERE categoria = '"+ categoria+"';", null);
		return  c.getInt(0);
	}
	
	public int getIdTipo(String tipo){
	
		Cursor c = banco.rawQuery("SELECT _id FROM TabelaTipo WHERE tipo = '"+ tipo+"';", null);
		c.moveToFirst();
		return  c.getInt(0);
	}
	
	public void alterarRegistro(long id, String desc, int cat,int tipo, String valor,String data) {
		ContentValues valores = new ContentValues();
		/*valores.put("descricao", desc);
		valores.put("quantidade", qtd);
		valores.put("grupo", grupo);*/
		
		valores.put("descricao",desc);
		valores.put("data", data);
		valores.put("valor",valor);
		valores.put("categoria", cat);
		valores.put("tipo", tipo);
		
		
		banco.update("financas", valores, "_id = " + id, null);
	}
	
	public Cursor listaQuantidadePorCategoria(){
		return banco.rawQuery("SELECT SUM(a.valor),b.categoria " +
							  "FROM financas a,TabelaCategoria b " +
							  "WHERE a.categoria = b._id " +
							  "GROUP BY b.categoria;", null);
		
	}
	
	
	
}
