package com.example.gestorfinanceiro;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class OpcaoMenuActivity extends ListActivity{
	
	private ArrayAdapter<String> opcao;
	private int tipo_cadastro ;
	private String[] menu = new String[]{
			"Cadastrar",
			"Editar/Excluir",
			"Voltar"};
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Intent it = getIntent();
		Bundle b = getIntent().getExtras();
		
		tipo_cadastro = b.getInt("tipo_cadastro");
		//Toast.makeText(this, "Tipo cadastro: "+id, Toast.LENGTH_SHORT).show();
		super.onCreate(savedInstanceState);
		opcao = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,menu);
		this.setListAdapter(opcao);
	}
	
	@Override
	protected void onListItemClick(ListView l,View v,int position,long id){
		if(position ==2){
			this.finish();
			return;
			}
		
		Intent it;
		
		super.onListItemClick(l,v,position,id);
		Object obj = this.getListAdapter().getItem(position);
		String menu = obj.toString();
	
	
		Toast.makeText(this, "Voce selecionou "+menu, Toast.LENGTH_SHORT).show();
		if(tipo_cadastro == 1){
			switch(position){
			case 0:
				it = new Intent(this,CadCredActivity.class);
				break;
			case 1:
				it = new Intent(this,Altera_Exclui.class);
				it.putExtra("tipo_cadastro", 1);
				break;
			default:
				it = new Intent(this,Altera_Exclui.class);
				break;
			
			}
		}else{
			switch(position){
			case 0:
				it = new Intent(this,CadDebActivity.class);
				break;
			case 1:
				it = new Intent(this,Altera_Exclui.class);
				it.putExtra("tipo_cadastro", 2);
				break;
			default:
				it = new Intent(this,Altera_Exclui.class);
				break;
			
			}
			
		}
		startActivity(it);
		
	}
	
}
