package com.example.gestorfinanceiro;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class CadCredActivity extends Activity {
	DAO dao;
	Spinner spinner1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cad_cred);
		
		dao = new DAO(this);
		dao.abrir();
		
		spinner1 =(Spinner)findViewById(R.id.spnCategoria);
		SimpleCursorAdapter ad = new SimpleCursorAdapter(this,
									android.R.layout.simple_list_item_1,
									dao.listarCategoria(),
									new String[]{"categoria"},
									new int[]{android.R.id.text1},0);
		spinner1.setAdapter(ad);
		
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.cad_cred, menu);
		return true;
	}
	
	public void onClick(View v){
		
		switch(v.getId()){
		
		case R.id.btnSalvar:
			
			EditText edtDescricao = (EditText)findViewById(R.id.edtDescricao);
			EditText edtData      = (EditText)findViewById(R.id.edtData);
			EditText edtValor     = (EditText)findViewById(R.id.edtValor);
			
			
			if (edtDescricao.getText().toString().equals("")){
				Toast.makeText(this,"Preencha todos os campos !", Toast.LENGTH_SHORT).show();
				break;
			}
			if (edtData.getText().toString().equals("")){
				Toast.makeText(this,"Preencha todos os campos !", Toast.LENGTH_SHORT).show();
				break;
			}
			if (edtValor.getText().toString().equals("")){
				Toast.makeText(this,"Preencha todos os campos !", Toast.LENGTH_SHORT).show();
				break;
			}
			
			String descricao = edtDescricao.getText().toString();
			String data      = edtData.getText().toString();
			Double valor     = Double.parseDouble(edtValor.getText().toString());
			
			dao.cadastrarFinanca(descricao, data, 1, (int)spinner1.getSelectedItemId(), valor);
			
			Toast.makeText(this, "Cadastrado com Sucesso!", 
					Toast.LENGTH_SHORT).show();
			/*
			Toast.makeText(this, "des: "+descricao+"spinner :"+spinner1.getSelectedItemId(), 
					Toast.LENGTH_LONG).show();
			*/
			this.finish();
			break;
					
		case R.id.btnSair:
			
			this.finish();
			
			break;
		
		}
		
	}

}
