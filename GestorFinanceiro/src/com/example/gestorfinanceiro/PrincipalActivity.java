package com.example.gestorfinanceiro;

import java.util.ArrayList;

import org.achartengine.ChartFactory;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


public class PrincipalActivity extends ListActivity {
	private ArrayAdapter<String> opcao;
	
	private String[] menu = new String[]{
			"Cadastrar Categoria D�bito/Cr�dito",
			"Cadastrar/Editar Cr�dito Mensal",
			"Casdatrar/Editar D�bito Mensal",
			"Listar Cr�dito Mensal",
			"Listar D�bito Mensal",
			"Or�amento Final",
			"Graficos"};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Toast.makeText(PrincipalActivity.this, "Voce selecionou nada krai", Toast.LENGTH_LONG).show();
		opcao = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,menu);
		this.setListAdapter(opcao);
	}
	
	
	
	
	@Override
	protected void onListItemClick(ListView l,View v,int position,long id){
		Intent it;
		super.onListItemClick(l,v,position,id);
		Object obj = this.getListAdapter().getItem(position);
		String menu = obj.toString();
		Toast.makeText(PrincipalActivity.this, "Voce selecionou "+menu, Toast.LENGTH_LONG).show();
		
		switch(position){
		case 0:
			it = new Intent(this,CadastroCategoria.class);
			
			break;
		case 1://cadastro credito
			//it = new Intent(this,CadCredActivity.class);
			it = new Intent(this,OpcaoMenuActivity.class);
			it.putExtra("tipo_cadastro", 1); 
			//it.putExtra("tipo_cadastro", 1);
			
			break;
		case 2:
			//it = new Intent(this,CadDebActivity.class);
			it = new Intent(this,OpcaoMenuActivity.class);
			it.putExtra("tipo_cadastro", 2);
			//it.putExtra("tipo_cadastro", 2);
			break;
		case 3:
			it = new Intent(this,ListaCredito.class);
			
			break;
		case 4:
			it = new Intent(this,ListaDebito.class);
			break;
		case 5:
			it = new Intent(this,OrcamentoFinalActivity.class);
			break;
			
		case 6:
			DAO dao = new DAO(this);
			ArrayList<String> grupos = new ArrayList<String>();
			ArrayList<Integer> valores =  new ArrayList<Integer>();
			
			dao.abrir();
			
			Cursor c  = dao.listaQuantidadePorCategoria();
			
			while(c.moveToNext()){
				grupos.add(c.getString(1));
				valores.add(c.getInt(0));
			}
			
			int[] cores = {Color.BLUE,Color.GREEN,Color.MAGENTA,Color.YELLOW,Color.WHITE};
			
			CategorySeries series = new CategorySeries("Produto em Grupo");
			
			for(int i=0; i< grupos.size();i++){
				series.add(grupos.get(i),(double)valores.get(i));
			}
			
			DefaultRenderer renderizador = new DefaultRenderer();
			
			for(int i =0;i<grupos.size();i++){
				SimpleSeriesRenderer s = new SimpleSeriesRenderer();
				s.setColor(cores[i]);
				s.setDisplayChartValues(true);
				s.setChartValuesTextSize(20);
				s.setShowLegendItem(true);
				renderizador.addSeriesRenderer(s);
			}
			renderizador.setLabelsTextSize(30);
			renderizador.setLegendTextSize(25);
			renderizador.setZoomButtonsVisible(true);
			it = ChartFactory.getPieChartIntent(this, series, renderizador, "Grafico Pizza");
			break;
			
		default:
			it = new Intent(this,Altera_Exclui.class);
			break;
		
		}
		startActivity(it);
		
	}
	
	
	

	

}
