package com.example.gestorfinanceiro;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;
import android.widget.Toast;

public class Altera_Exclui extends ListActivity {
	DAO dao ;
	int tipo_cadastro;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Intent it = getIntent();
		Bundle b = getIntent().getExtras();
		
		tipo_cadastro = b.getInt("tipo_cadastro");
		
		Toast.makeText(this, "Para editar deixe apertado", Toast.LENGTH_LONG).show();
		dao = new DAO(this);
		dao.abrir();
		listar();
		//Toast.makeText(this, "total de debitos: "+ dao.totalDebito(),Toast.LENGTH_LONG).show();
		//Toast.makeText(this, "total de Cr�dito : "+ dao.totalCredito(),Toast.LENGTH_LONG).show();
		
		//dao.fechar();
		
	}
	public void listar(){
		//verificar se � credito ou debito
		SimpleCursorAdapter adaptador;
		if(tipo_cadastro == 1){
			adaptador = new SimpleCursorAdapter(this,
				R.layout.financas_activity,dao.listarCredito(), 
				new String[]{"descricao","data","valor"}, 
				new int[]{R.id.txtDescricao,R.id.txtData,R.id.txtValor});
		}else{
			 adaptador = new SimpleCursorAdapter(this,
					R.layout.financas_activity,dao.listarDebito(), 
					new String[]{"descricao","data","valor"}, 
					new int[]{R.id.txtDescricao,R.id.txtData,R.id.txtValor});
			
		}
		setListAdapter(adaptador);
		getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		
		registerForContextMenu(getListView());

		
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	        ContextMenuInfo menuInfo) {
	    super.onCreateContextMenu(menu, v, menuInfo);
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.menu, menu);
	    
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
	    AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
	            .getMenuInfo();
	 
	    switch (item.getItemId()) {
	    case R.id.alterar:
	    	Toast.makeText(this, "alterar:"+info.id, Toast.LENGTH_SHORT).show();
	    	Intent it = new Intent(this,AlterarRegistroActivity.class);
	    	it.putExtra("id", info.id);
	    	startActivity(it);
	    	//listar();
	    	break;
	    case R.id.remover:
	    	Toast.makeText(this, "remover:"+info.id, Toast.LENGTH_SHORT).show();
	    	dao.excluirFinanca((int)info.id);
	    	
	       break;
	    }
	    
	    listar();
	    this.finish();
	    
	    return true;
	}
	
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {		
		super.onListItemClick(l, v, position, id);
		 
		// Toast.makeText(this, "selecionou opcao:  "+id, Toast.LENGTH_SHORT).show();
		 
		 /*
		Toast.makeText(this, "Descri��o: " + String.valueOf(position + 1) + 
				": " + l.getItemAtPosition(position).toString(),
				Toast.LENGTH_SHORT).show();*/
		
		
		
		
	}
	
	
	
}
